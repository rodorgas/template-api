const { v4: uuid } = require("uuid");
const { validator } = require("../validator")

module.exports = async (req, res) => {
  const { db, stanConn } = req.app.locals

  const validatorResult = validator(req.body, {
    name: { required: true, type: 'string' },
    email:  { required: true, type: 'email' },
    password: { required: true, min: 8, max: 32, type: 'string' },
    passwordConfirmation: {
      required: true, min: 8, max: 32, type: 'string'
    },
  })

  if (validatorResult !== 'OK') {
    res.status(400).send(validatorResult + '\n')
    return
  }

  if (req.body.password !== req.body.passwordConfirmation) {
    res.status(422).send('Password confirmation did not match\n')
    return
  }

  const previousUser = await db.collection('users').findOne({ email: req.body.email })
  if (previousUser) {
    res.status(400).send('Email is already registered\n')
    return
  }

  const userId = uuid()

  stanConn.publish('users', JSON.stringify({
    eventType: 'UserCreated',
    entityId: userId,
    entityAggregate: {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    },
  }))

  res.status(201).send({
    user : {
      id: userId,
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    }
  })
}

