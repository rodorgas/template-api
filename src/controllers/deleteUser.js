const jwt = require("jsonwebtoken");

module.exports = async (req, res) => {
  const { db, stanConn } = req.app.locals

  if (req.get('Authentication') === undefined) {
    res.status(401).send({
      "error": "Access Token not found",
    })
    return
  }

  const uuid = jwt.decode(req.get('Authentication').split(' ')[1]).id

  if (uuid !== req.params.uuid) {
    res.status(403).send({
      "error": "Access Token did not match User ID",
    })
    return
  }

  stanConn.publish('users', JSON.stringify({
    eventType: 'UserDeleted',
    entityId: uuid,
    entityAggregate: {},
  }))

  res.status(200).send({ "status": "ok" })
}
