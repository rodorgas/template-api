const express = require("express");
const cors = require("cors");
const createUser = require('./controllers/createUser')
const deleteUser = require('./controllers/deleteUser')

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.locals.db =  mongoClient.db("pingr")
  api.locals.stanConn =  stanConn
  api.locals.secret = secret

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (_, res) => res.json("Hello, World!"));

  api.post("/users", createUser)
  api.delete("/users/:uuid", deleteUser)

  return api;
};
