const validator = (data, schema) => {
  const errors = {
    missing: (field) => `Request body had missing field ${field}`,
    malformed: (field) => `Request body had malformed field ${field}`
  }

  const checkType = {
    string: (value) => typeof value === 'string',
    email: (value) => /\S+@\S+\.\S+/.test(value),
  }

  for ([field, options] of Object.entries(schema)) {
    const isPresent = data[field] !== undefined
    const isRequired = options['required']

    if (isRequired && !isPresent) {
      return errors.missing(field)
    }

    if (isPresent) {
      for ([option, value] of Object.entries(options)) {
        if (option === 'type' && !checkType[value](data[field])) {
          return errors.malformed(field)
        }
        else if (option === 'min' && data[field].length < value) {
          return errors.malformed(field)
        }
        else if (option === 'max' && data[field].length > value) {
          return errors.malformed(field)
        }
      }
    }
  }

  return 'OK'
}

module.exports = { validator }
