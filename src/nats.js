const stan = require("node-nats-streaming");

const createUser = (user, mongoClient) => {
  const db = mongoClient.db("pingr")
  return db.collection('users').insertOne({
    ...user,
    _id: user.uuid,
  })
}

const deleteUser = (uuid, mongoClient) => {
  const db = mongoClient.db("pingr")
  return db.collection("users").deleteOne({
    _id: uuid,
  })
}

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", async () => {
    console.log("Connected to NATS Streaming");

    const opts = conn.subscriptionOptions();
    opts.setStartWithLastReceived()

    const subscription = conn.subscribe('users', opts)
    subscription.on('message', async (msg) => {
      msg.ack()
      const message = JSON.parse(msg.getData())

      if (message.eventType === 'UserCreated') {
        createUser({
          ...message.entityAggregate,
          uuid: message.entityId
        }, mongoClient)
      }
      else if (message.eventType === 'UserDeleted') {
        deleteUser({
          uuid: message.entityId
        }, mongoClient)
      }
    })
  });

  return conn
};
